package com.citi.testing;

import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

public class TestLifecycle {
    
    public static String mode;
    public String condition;

    @BeforeClass
    public static void setUpClass() {
        mode = "standard";
        System.out.format("@BeforeClass method invoked: mode %s.%n", mode);
    }
    
    @Before
    public void setUp() {
        condition = "normal";
        System.out.format("  @Before method invoked: mode %s condition %s.%n", mode, condition);
    }
    
    @Test
    public void test1() {
        System.out.format("    test1() invoked: mode %s condition %s.%n", mode, condition);
        mode = "accelerated";
        condition = "hot";
    }
    
    @Test
    public void test2() {
        System.out.format("    test2() invoked: mode %s condition %s.%n", mode, condition);
        mode = "relaxed";
        condition = "cold";
    }
    
    @After
    public void tearDown() {
        System.out.format("  @After method invoked: mode %s condition %s.%n", mode, condition);
}
    
    @AfterClass
    public static void tearDownClass() {
        System.out.format("@AfterClass method invoked: mode %s.%n", mode);
    }
}