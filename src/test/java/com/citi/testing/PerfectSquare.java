/*
Copyright 2016 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package com.citi.testing;

import org.hamcrest.Description;
import org.hamcrest.TypeSafeMatcher;

/**
Custom matcher that tests if an integer is the square of any other integer.

@author Will Provost
*/
public class PerfectSquare
    extends TypeSafeMatcher<Number>
{
    /**
    Check to see that the number is the square of the integer
    portion of its own square root.
    */
    public boolean matchesSafely (Number number)
    {
        double value = number.doubleValue ();
        long root = (long) Math.sqrt (value);
        return root * root == value;
    }

    /**
    Provides a simple text description.
    */
    public void describeTo (Description description)
    {
        description.appendText ("perfect square");
    }

    /**
    Following the fluent style of Hamcrest matchers, we provide this
    factory method as a self-descriptive way to invoke this matcher.
    */
    public static PerfectSquare perfectSquare ()
    {
        return new PerfectSquare ();
    }
}
