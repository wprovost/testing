package com.conygre.spring.rest;

import static org.hamcrest.MatcherAssert.assertThat;
import static org.hamcrest.Matchers.equalTo;

import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.spring.data.CompactDiscRepository;
import com.conygre.spring.entities.CompactDisc;
import com.conygre.spring.service.CompactDiscService;

import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.PropertySource;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringRunner;

@RunWith(SpringRunner.class)
@ContextConfiguration(classes=CompactDiscControllerIntegrationTestSeparateDB.Config.class)
public class CompactDiscControllerIntegrationTestSeparateDB {

    @SpringBootApplication(scanBasePackageClasses={
            CompactDiscController.class,
            CompactDiscService.class
        })
    @EnableMongoRepositories(basePackages = "com.conygre.spring.data")
    @PropertySource("file:src/test/resources/test.properties")
    public static class Config {
    }
    @Autowired
    private CompactDiscController controller;


    @Autowired
    private CompactDiscRepository repo;

    public String testID;

    @Before
    public void setUp() {
        repo.deleteAll();

        CompactDisc disc = new CompactDisc("test title", "test artist", 100);
        testID = repo.save(disc).getId().toString();
    }
    
    @Test
    public void testFindAll() {
        Iterable<CompactDisc> CDs = controller.findAll();
        Stream<CompactDisc> stream = StreamSupport.stream(CDs.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }
    
    @Test
    public void testCdById() {
        Optional<CompactDisc> CD = controller.getCdById(testID);
        assertThat(CD.isPresent(), equalTo(true));
    }
}