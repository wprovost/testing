package com.conygre.spring.rest;

import static org.hamcrest.Matchers.*;
import static org.hamcrest.MatcherAssert.assertThat;
import static org.mockito.Mockito.mock;
import static org.mockito.Mockito.when;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Stream;
import java.util.stream.StreamSupport;

import com.conygre.spring.entities.CompactDisc;
import com.conygre.spring.service.CompactDiscService;

import org.bson.types.ObjectId;
import org.junit.Before;
import org.junit.Test;

public class CompactDiscControllerUnitTestNoSpring {
    

    public static final String TEST_ID = "5f46a3d545bee629d17fd7b2";

    private CompactDiscController controller;
    
    @Before
    public void setUp() {
        ObjectId ID = new ObjectId(TEST_ID);
        CompactDisc CD = new CompactDisc("", "", 0);
        List<CompactDisc> CDs = new ArrayList<>();
        CDs.add(CD);

        CompactDiscService service = mock(CompactDiscService.class);
        when(service.getCatalog()).thenReturn(CDs);
        when(service.getCompactDiscById(ID)).thenReturn(Optional.of(CD));

        controller = new CompactDiscController(service);
    }

    @Test
    public void testFindAll() {
        Iterable<CompactDisc> CDs = controller.findAll();
        Stream<CompactDisc> stream = StreamSupport.stream(CDs.spliterator(), false);
        assertThat(stream.count(), equalTo(1L));
    }
    
    @Test
    public void testCdById() {
        Optional<CompactDisc> CD = controller.getCdById(TEST_ID);
        assertThat(CD.isPresent(), equalTo(true));
    }
 
}