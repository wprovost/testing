/*
Copyright 2016 Will Provost.
All rights reserved by Capstone Courseware, LLC.
*/

package com.citi.testing;

import java.util.logging.Logger;

/**
 * Simple JavaBean for use in exploring different object matchers.
 * 
 * @author Will Provost
 */
public class MyBean
{
    private static Logger log = Logger.getLogger(MyBean.class.getName());

    private String name;
    private int number;
    private MyBean delegate;

    /**
    Leaves name and delegate fields null, and number at zero.
    */
    public MyBean ()
    {
        log.info("MyBean instantiated.");
    }

    /**
    Sets name and number, leaves delegate null.
    */
    public MyBean (String name, int number)
    {
        this.name = name;
        this.number = number;
    }

    /**
    Accessor for the name.
    */
    public String getName ()
    {
        return name;
    }

    /**
    Mutator for the name.
    */
    public void setName (String name)
    {
        this.name = name;
    }

    /**
    Accessor for the number.
    */
    public int getNumber ()
    {
        return number;
    }

    /**
    Mutator for the number.
    */
    public void setNumber (int number)
    {
        this.number = number;
    }

    /**
    Accessor for the delegate.
    */
    public MyBean getDelegate ()
    {
        return delegate;
    }

    /**
    Mutator for the delegate.
    */
    public void setDelegate (MyBean delegate)
    {
        this.delegate = delegate;
    }

    /**
    For general purposes, we consider two instances to be equivalent
    if they have the same name and number.
    */
    @Override
    public boolean equals (Object other)
    {
        if (!(other instanceof MyBean))
            return false;

        MyBean otherBean = (MyBean) other;
        return otherBean.getName ().equals (name) &&
            otherBean.getNumber () == number;
    }
}
