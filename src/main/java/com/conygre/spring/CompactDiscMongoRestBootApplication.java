package com.conygre.spring;

import java.util.logging.Logger;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

@SpringBootApplication
@EnableMongoRepositories(basePackages = "com.conygre.spring.data")
public class CompactDiscMongoRestBootApplication {

	private static Logger log = Logger.getLogger(CompactDiscMongoRestBootApplication.class.getName());

	public static void main(String[] args) {
	// 	MyBean bean = new MyBean();
	// 	try {
	// 		if (bean.getName().length() != 0) {
	// 			System.out.println("Nonzero length.");
	// 		}
	// 	} catch (Exception ex) {
	// 		log.log(Level.SEVERE, "It blew up!", ex);
	// 	}

		SpringApplication.run(CompactDiscMongoRestBootApplication.class, args);
	}

}
